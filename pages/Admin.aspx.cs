﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class pages_Admin : System.Web.UI.Page
{
    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
    SqlConnection sqlConn = new SqlConnection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin"] == null)
            Response.Redirect("dang-nhap.aspx");
        sqlConn.ConnectionString = ConnectionString;
        if (!IsPostBack)
        {
            RepterDetails();
        }
    }
    public void RepterDetails()
    {
        try
        {
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = sqlConn;
            sqlCmd.CommandText = "select * from ThiSinh Where GroupID != 0";
            SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd);
            DataSet ds = new DataSet();
            sqlDA.Fill(ds);
            clPager.PageSize = 15;
            clPager.DataSource = ds.Tables[0].DefaultView;
            clPager.BindToControl = Repter_Details;
            Repter_Details.DataSource = clPager.DataSourcePaged;
            Repter_Details.DataSource = ds;
            Repter_Details.DataBind();
        }
        catch(Exception ex)
        {

        }
    }
}