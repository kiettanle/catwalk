﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class pages_sua_thong_tin_thi_sinh : System.Web.UI.Page
{
    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
    SqlConnection sqlConn = new SqlConnection();
    string MaThiSinh;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin"] == null)
            Response.Redirect("dang-nhap.aspx");
        MaThiSinh = Request.QueryString["id"];
        sqlConn.ConnectionString = ConnectionString;
        if (!IsPostBack)
        {
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = sqlConn;
            sqlCmd.CommandText = "select * from ThiSinh  where MaThiSinh = " + MaThiSinh;
            SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd);
            DataSet DS = new DataSet();
            sqlDA.Fill(DS);
            txtHoTen.Text = DS.Tables[0].Rows[0]["HoTen"].ToString();
            txtChieuCao.Text = DS.Tables[0].Rows[0]["ChieuCao"].ToString();
            txtCanNang.Text = DS.Tables[0].Rows[0]["CanNang"].ToString();
            txtEmail.Text = DS.Tables[0].Rows[0]["Email"].ToString();
            if (DS.Tables[0].Rows[0]["GioiTinh"].ToString() == "Nam")
                radNam.Checked = true;
            else
                radNu.Checked = true;
            txtNamSinh.Text = DS.Tables[0].Rows[0]["NamSinh"].ToString();
            txtSoDienThoai.Text = DS.Tables[0].Rows[0]["SoDienThoai"].ToString();

        }
    }
    //Xử lý nút đăng ký
    public void btnSua_Click(object sender, EventArgs e)
    {
        try
        {
            string gt = "";
            try
            {
                sqlConn.Open();
            }
            catch (SqlException ex)
            {
                Response.Write("<script>alert('Kết nối không thành công !')</script>");
            }

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = sqlConn;
            SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd);
            DataTable dtThanhVien = new DataTable();
            if (radNam.Checked == true)
                gt = radNam.Text;
            else
                gt = radNu.Text;
            sqlCmd.CommandText = "Update ThiSinh set HoTen = N'" + txtHoTen.Text
                + "',NamSinh = N'" + txtNamSinh.Text
                + "' ,GioiTinh = N'" + gt
                + "' ,ChieuCao = N'" + txtChieuCao.Text
                 + "' ,CanNang = N'" + txtCanNang.Text
                + "',Email = N'" + txtEmail.Text
                + "',SoDienThoai = '" + txtSoDienThoai.Text + "' where MaThiSinh= " + MaThiSinh;
            sqlDA.Fill(dtThanhVien);

            try
            {
                sqlCmd.ExecuteNonQuery();
                Response.Write("<script>alert('Sửa thành công !')</script>");
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('Sửa không thành công !')</script>");
            }
        }
        catch(Exception ex)
        {

        }
    }
}