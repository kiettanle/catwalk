﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Home.master" AutoEventWireup="true" CodeFile="catwalk.aspx.cs" Inherits="pages_catwalk" EnableEventValidation="false" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--row -->
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <img src="../Images/header.jpg" title="Công đoàn trường đại học Sư phạm Kỹ thuật Tp.HCM" alt="#" />
            </div>
            <h2 style="color: darkblue">
                <center>
                    <b>
                    ĐĂNG KÝ THAM GIA BIỂU DIỄN THỜI TRANG CHARMING HCMUTE 2015
                      
                    </b>
                </center>
            </h2>
            <hr />
        </div>
    </div>
    <!-- /.row -->
    <!--row -->
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10 col-lg-push-1">
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading text-center" style="color: white; font-weight: bold">ĐĂNG KÝ THAM GIA CUỘC THI ẢNH ĐẸP CHARMING HCMUTE 2015</div>
                    <div class="panel-body">
                        <div class="form">
                            <div>
                                <div class="col-lg-10 col-lg-push-1 ">
                                    <asp:Label
                                        ID="Chon"
                                        Text="Tên đội"
                                        runat="server"
                                        Font-Bold="true"
                                        ForeColor="darkblue"
                                        Font-Size="Large" />
                                    <br />
                                    <asp:TextBox ID="txtTenDoi" runat="server" CssClass="form-control" placeholder="Tên đội của bạn..." MaxLength="50"></asp:TextBox>
                                </div>
                                <div>
                                    <div class="col-lg-5 col-lg-push-1">
                                        <fieldset>
                                            <asp:Label
                                                ID="thisinh1"
                                                Text="Thông tin thí sinh 1"
                                                runat="server"
                                                Font-Bold="true"
                                                ForeColor="red"
                                                Font-Size="Large" />
                                            <div class="form-group">
                                                <asp:Label
                                                    ID="hoten"
                                                    Text="Họ và tên thí sinh"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    ForeColor="darkblue"
                                                    Font-Size="Large" />
                                                <asp:TextBox ID="txtHoTen1" runat="server" placeholder="Họ tên của bạn..." CssClass="form-control" MaxLength="50" required autofocus></asp:TextBox>
                                                <asp:Label
                                                    ID="gioitinh"
                                                    Text="Giới tính"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    ForeColor="darkblue"
                                                    Font-Size="Large" />
                                                <br />
                                                <asp:RadioButton
                                                    ID="radNam1"
                                                    runat="server"
                                                    Text="Nam"
                                                    GroupName="GioiTinh1"
                                                    Font-Bold="true"
                                                    Checked="true"
                                                    ForeColor="Navy" />
                                                &nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton
                                                    ID="radNu1"
                                                    runat="server"
                                                    Text="Nữ"
                                                    GroupName="GioiTinh1"
                                                    Font-Bold="true"
                                                    ForeColor="Navy" />
                                                <br />
                                                <asp:Label
                                                    ID="namsinh"
                                                    Text="Năm sinh"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    ForeColor="darkblue"
                                                    Font-Size="Large" />
                                                <asp:TextBox ID="txtNamSinh1" runat="server" placeholder="Ví dụ: 1990" type="number" MaxLength="4" Min="1950" Max="1994" CssClass="form-control" required></asp:TextBox>
                                                <asp:Label
                                                    ID="chieucao1"
                                                    Text="Chiều cao (cm)"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    ForeColor="darkblue"
                                                    Font-Size="Large" />
                                                <asp:TextBox ID="txtChieuCao1" runat="server" placeholder="Chiều cao của bạn..." type="number" Min="100" Max="250" CssClass="form-control" required></asp:TextBox>
                                                <asp:Label
                                                    ID="cannang1"
                                                    Text="Cân nặng (kg)"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    ForeColor="darkblue"
                                                    Font-Size="Large" />
                                                <asp:TextBox ID="txtCanNang1" runat="server" placeholder="Cân nặng của bạn..." type="number" Min="40" step="0.1" CssClass="form-control" required></asp:TextBox>
                                                <asp:Label
                                                    ID="Email"
                                                    Text="Email"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    ForeColor="darkblue"
                                                    Font-Size="Large" />
                                                <asp:TextBox ID="txtEmail1" type="email" runat="server" placeholder="Nhập email của bạn..." CssClass="form-control" MaxLength="50" required></asp:TextBox>

                                                <asp:Label
                                                    ID="sodienthoai"
                                                    Text="Số điện thoại"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    ForeColor="darkblue"
                                                    Font-Size="Large" />
                                                <asp:TextBox ID="txtSoDienThoai1" type="text" runat="server" placeholder="Số điện thoại..." MaxLength="11" CssClass="form-control" required></asp:TextBox>
                                                <br />
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-5 col-lg-push-1">
                                        <fieldset>
                                            <asp:Label
                                                ID="thisinh2"
                                                Text="Thông tin thí sinh 2"
                                                runat="server"
                                                Font-Bold="true"
                                                ForeColor="red"
                                                Font-Size="Large" />
                                            <br />
                                            <asp:Label
                                                ID="Label1"
                                                Text="Họ và tên thí sinh"
                                                runat="server"
                                                Font-Bold="true"
                                                ForeColor="darkblue"
                                                Font-Size="Large" />
                                            <asp:TextBox ID="txtHoTen2" runat="server" placeholder="Họ tên của bạn..." CssClass="form-control" MaxLength="50" required></asp:TextBox>
                                            <asp:Label
                                                    ID="Label3"
                                                    Text="Giới tính"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    ForeColor="darkblue"
                                                    Font-Size="Large" />
                                                <br />
                                                <asp:RadioButton
                                                    ID="radNam2"
                                                    runat="server"
                                                    Text="Nam"
                                                    GroupName="GioiTinh2"
                                                    Font-Bold="true"
                                                    ForeColor="Navy" />
                                                &nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton
                                                    ID="radNu2"
                                                    runat="server"
                                                    Text="Nữ"
                                                    GroupName="GioiTinh2"
                                                    Font-Bold="true"
                                                    Checked="true"
                                                    ForeColor="Navy" />
                                                <br />
                                            
                                            <asp:Label
                                                ID="Label2"
                                                Text="Năm sinh"
                                                runat="server"
                                                Font-Bold="true"
                                                ForeColor="darkblue"
                                                Font-Size="Large" />
                                            <asp:TextBox ID="txtNamSinh2" runat="server" placeholder="Ví dụ: 1989" type="number" Max="1994" Min="1950" CssClass="form-control" required></asp:TextBox>
                                            <asp:Label
                                                ID="chieucao2"
                                                Text="Chiều cao (cm)"
                                                runat="server"
                                                Font-Bold="true"
                                                ForeColor="darkblue"
                                                Font-Size="Large" />
                                            <asp:TextBox ID="txtChieuCao2" runat="server" placeholder="Chiều cao của bạn..." type="number" Min="100" Max="250" CssClass="form-control" required></asp:TextBox>
                                            <asp:Label
                                                ID="cannang2"
                                                Text="Cân nặng (kg)"
                                                runat="server"
                                                Font-Bold="true"
                                                ForeColor="darkblue"
                                                Font-Size="Large" />
                                            <asp:TextBox ID="txtCanNang2" runat="server" placeholder="Cân nặng của bạn..." type="number" Min="40" step="0.1" CssClass="form-control" required></asp:TextBox>
                                            <asp:Label
                                                ID="Label5"
                                                Text="Email"
                                                runat="server"
                                                Font-Bold="true"
                                                ForeColor="darkblue"
                                                Font-Size="Large" />
                                            <asp:TextBox ID="txtEmail2" type="email" runat="server" placeholder="Nhập email của bạn..." CssClass="form-control" MaxLength="50" required></asp:TextBox>

                                            <asp:Label
                                                ID="Label6"
                                                Text="Số điện thoại"
                                                runat="server"
                                                Font-Bold="true"
                                                ForeColor="darkblue"
                                                Font-Size="Large" />
                                            <asp:TextBox ID="txtSoDienThoai2" type="text" runat="server" placeholder="Số điện thoại..." CssClass="form-control" MaxLength="11" required></asp:TextBox>
                                            <br />
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" type="submit" Text="Đăng ký" OnClick="btnSubmit_Click" class="btn-block btn-primary btn-lg" />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Bootstrap Modal Dialog -->
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">
                                <asp:Label ID="lblModalTitle" runat="server" Text="" style="color:darkblue; font-weight:bold; font-size:medium"></asp:Label></h4>
                        </div>
                        <div class="modal-body" style="color:red; font-weight:bold; font-size:medium">
                            <asp:Label ID="lblModalBody" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Đóng</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- /.row -->
</asp:Content>


