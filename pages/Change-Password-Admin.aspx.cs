﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
public partial class pages_Edit_Pass : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin"] == null)
            Response.Redirect("dang-nhap.aspx");
        try
        {
            if (!Page.IsPostBack)
            {
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
                SqlConnection sqlConn = new SqlConnection(ConnectionString);
                try
                {
                    sqlConn.Open();
                }
                catch (SqlException ex)
                {
                    Response.Write("<script>alert('Kết nối không thành công !')</script>");
                }
                try
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = sqlConn;
                    sqlCmd.CommandText = "Select * from Users where UserID = 1" ;
                    SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd);
                    DataSet DS = new DataSet();
                    sqlDA.Fill(DS);
                    txt_Name.Text = DS.Tables[0].Rows[0]["UserName"].ToString();
                    Label lblMasterUsername = (Label)Master.FindControl("lbl_names");
                    lblMasterUsername.Text = DS.Tables[0].Rows[0]["UserName"].ToString();
                }
                catch (Exception ex)
                {
                    Response.Write("<script>alert('Lỗi không xác định !')</script>");
                }

            }
        }
        catch (Exception ex)
        {

        }

    }
    protected void btnSuaPass_Click(object sender, EventArgs e)
    {
        try
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(ConnectionString);
            try
            {
                sqlConn.Open();
            }
            catch (SqlException)
            {
                Response.Write("<script>alert('Kết nối không thành công!')</script>");
            }
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = sqlConn;
            sqlCmd.CommandText = "Select * from Users where UserID = 1";
            SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd);
            DataSet DS = new DataSet();
            sqlDA.Fill(DS);
            string MatKhauCu = DS.Tables[0].Rows[0]["PassWord"].ToString();
            if (txt_PassCu.Text != MatKhauCu)
            {
                lbtNotice.Text = "Mật khẩu cũ không đúng, vui lòng nhập lại!";
                return;
            }
            sqlCmd.CommandText = "Update Users set  PassWord = N'" + txt_pass_new.Text + "' where UserID = 1";
            try
            {

                sqlCmd.ExecuteNonQuery();
                lbtNotice.Text = "Cập nhật mật khẩu mới thành công !";
            }
            catch (Exception)
            {
                string script = "alert(\"Không cập nhật được mật khẩu !\");";
                ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", script, true);
            }
        }
        catch(Exception ex)
        {

        }
    }
}