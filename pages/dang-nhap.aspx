﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dang-nhap.aspx.cs" Inherits="pages_dang_nhap" EnableEventValidation="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 2.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Đăng nhập</title>
    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="login"></div>
        <div class="container" style="margin-top: 80px">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong>ĐĂNG NHẬP HỆ THỐNG</strong>
                        </div>
                        <div class="panel-body">
                            <form role="form" action="#" method="POST">
                                <fieldset>
                                    <div class="row">
                                        <div class="center-block">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-10  col-md-offset-1 ">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="glyphicon glyphicon-envelope"></i>
                                                    </span>
                                                    <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Email ..." type="email" autofocus></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="glyphicon glyphicon-lock"></i>
                                                    </span>
                                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" class="form-control" placeholder="Password ..." type="password"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Button ID="btnAccept" runat="server" Text="Đăng nhập"
                                                    class="btn btn-lg btn-primary btn-block" OnClick="btnAccept_Click" />
                                                <a href="Default.aspx" class="btn btn-lg btn-danger btn-block">Thoát</a>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                            <div class="notice">
                                <asp:Label ID="lbtNotice" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---------------------------------------------->
        <div class="navbar navbar-default navbar-fixed-bottom">
            <div class="container">
                <p class="navbar-text pull-left" style="font-weight: bold; font-size: 11px;">
                    Copyright © 2015 - <font color="black">Công đoàn trường Đại học Sư phạm Kỹ thuật TP.HCM</font>
                </p>
                <a href="http://congdoan.hcmute.edu.vn/" class="navbar-btn btn-primary btn pull-right">
                    <span class="glyphicon glyphicon-star"></span>congdoan.hcmute.edu.vn</a>
            </div>
        </div>
    </form>
</body>
</html>
