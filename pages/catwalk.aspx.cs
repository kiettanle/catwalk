﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class pages_catwalk : System.Web.UI.Page
{
    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
    SqlConnection sqlConn = new SqlConnection();
    protected void Page_Load(object sender, EventArgs e)
    {
        sqlConn.ConnectionString = ConnectionString;
        if (!IsPostBack)
        {
        }
    }
    //Xử lý nút đăng ký
    public void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string gt1,gt2;
            if (radNam1.Checked == true)
                gt1 = "Nam";
            else
                gt1 = "Nữ";
            if (radNam2.Checked == true)
                gt2 = "Nam";
            else
                gt2 = "Nữ";
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(ConnectionString);
            try
            {
                sqlConn.Open();
            }
            catch (SqlException ex)
            {
                Response.Write("<script>alert('Kết nối không thành công!')</script>");
            }
            // kiểm tra điều kiện trùng lặp email
            string existmail = "Select * from ThiSinh where Email = '" + txtEmail1.Text + "' or  Email = '" + txtEmail2.Text + "' ";
            SqlCommand sqlCmd = new SqlCommand(existmail, sqlConn);
            SqlDataReader dr = sqlCmd.ExecuteReader();
            if (dr.Read())
            {
                Response.Write("<script>alert('Email của thí sinh 1 hoặc thí sinh 2 đã được sử dụng !')</script>");
            }
            //Email không trùng
            else
            {
                //Nếu là mail trường
                if (CheckMail(txtEmail1.Text) && CheckMail(txtEmail2.Text))
                {
                    try
                    {
                        dr.Close();
                        sqlCmd.Connection = sqlConn;
                        //Kiểm tra giới tính
                        //Lấy mã nhóm GroupID
                        SqlCommand GroupID = new SqlCommand();
                        GroupID.Connection = sqlConn;
                        GroupID.CommandText = "select max(GroupID) as GroupID from ThiSinh";
                        SqlDataAdapter sqlDAGroupID = new SqlDataAdapter(GroupID);
                        DataSet Nhom = new DataSet();
                        sqlDAGroupID.Fill(Nhom);
                        //Lấy mã số nhóm
                        int MaNhom = Convert.ToInt32(Nhom.Tables[0].Rows[0]["GroupID"].ToString()) + 1;

                        //Câu lệnh intert thí sinh vào database
                        SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd);
                        DataTable dtThanhVien = new DataTable();
                        sqlDA.Fill(dtThanhVien);
                        string thisinh1 =
                          " Insert into ThiSinh(HoTen,GioiTinh,NamSinh,ChieuCao,CanNang,Email,SoDienThoai,ThoiGianDangKy,GroupID,TenDoi) Values (N'"
                          + txtHoTen1.Text + "',N'" + gt1 + "',N'" + txtNamSinh1.Text + "', '"
                          + txtChieuCao1.Text + "' ,'" + txtCanNang1.Text + "',N'" + txtEmail1.Text + "',N'" + txtSoDienThoai1.Text + "', GETDATE()," + MaNhom + ",N'" + txtTenDoi.Text + "')";
                        sqlCmd.CommandText = thisinh1 +
                          " Insert into ThiSinh(HoTen,GioiTinh,NamSinh,ChieuCao,CanNang,Email,SoDienThoai,ThoiGianDangKy,GroupID,TenDoi) Values (N'"
                          + txtHoTen2.Text + "',N'" + gt2 + "',N'" + txtNamSinh2.Text + "', '"
                          + txtChieuCao2.Text + "' ,'" + txtCanNang2.Text + "',N'" + txtEmail2.Text + "',N'" + txtSoDienThoai2.Text + "', GETDATE()," + MaNhom + ",N'" + txtTenDoi.Text + "')";
                    }
                    catch (Exception ex)
                    {

                    }
                    try
                    {
                        sqlCmd.ExecuteNonQuery();
                        //Đoạn lệnh lấy ra mã số thí sinh, sau khi đăng ký, và dữ liệu được lưu xuống database
                        SqlCommand sqlCmd2 = new SqlCommand();
                        sqlCmd2.Connection = sqlConn;
                        sqlCmd2.CommandText = "select * from ThiSinh where Email = N'" + txtEmail1.Text + "'";
                        SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd2);
                        DataSet ThiSinh = new DataSet();
                        sqlDA.Fill(ThiSinh);
                        //Lấy mã số thí sinh
                        string MaSo = ThiSinh.Tables[0].Rows[0]["GroupID"].ToString();
                        //Trả về thông báo sau khi đăng ký hoàn tất
                        lblModalTitle.Text = "Chúc mừng, bạn đã đăng ký thành công !";
                        lblModalBody.Text = "Mã số cặp (đôi) của 2 bạn là: " + MaSo + " .Mời bạn xem thông tin của đội mình tại mục Danh sách đăng ký. Cảm ơn bạn đã đăng ký !";
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
                        upModal.Update();
                    }
                    catch (Exception ex)
                    {
                        Response.Write("<script>alert('Đăng ký không thành công !')</script>");
                    }
                }
                //Nếu không phải mail trường
                else
                {
                    Response.Write("<script>alert('Xin sử dụng Email của trường để đăng ký !')</script>");
                }

            }
        }
        catch(Exception ex)
        {

        }
    }
    //Hàm kiểm tra mail trường
    public bool CheckMail(string temp)
    {
        //Độ dài email dưới 17 ký tự vì @hcmute.edu.vn chiếm 14 ký tự
        if (temp.Length < 17)
        {
            Response.Write("<script>alert('Email không tồn tại trong hệ thống Email của trường !')</script>");
            return false;
        }
        else
        {
            try
            {
                //Lấy vị trí đầu tiên của chuỗi hcmute
                int vitri = temp.IndexOf("@hcmute");
                //Cắt chuỗi
                string hcmute = temp.Substring(vitri, temp.Length - vitri);
                //So sánh chuỗi đã cắt với @hcmute.edu.vn
                bool t = hcmute == "@hcmute.edu.vn";
                return t;
            }
            catch
            {
                Response.Write("<script>alert('Email không hợp lệ !')</script>");
            }
            return false;
        }
    }
}