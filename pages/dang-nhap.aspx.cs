﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class pages_dang_nhap : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //do something 
        }
    }
    protected void btnAccept_Click(object sender, EventArgs e)
    {
        try
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(ConnectionString);
            try
            {
                sqlConn.Open();
            }
            catch (SqlException ex)
            {
                Response.Write("<script>alert('Kết nối không thành công!')</script>");
            }
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = sqlConn;
            // truy vấn xem dữ liệu
            sqlCmd.CommandText = "Select * from Users where Email = '" + txtEmail.Text + "' and Password = '" + txtPassword.Text + "'";
            SqlDataAdapter sqlDA = new SqlDataAdapter(sqlCmd);
            DataTable dtThanhVien = new DataTable();
            sqlDA.Fill(dtThanhVien);
            if (dtThanhVien.Rows.Count > 0)
            {
                Session.Add("Admin","Admin");
                Response.Redirect("Admin.aspx");
            }
            else
                Response.Write("<script>alert('Email hoặc mật khẩu không đúng !')</script>");
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('Không thể đăng nhập. Lỗi !')</script>");
        }
    }
}