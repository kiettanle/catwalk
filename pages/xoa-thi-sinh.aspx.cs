﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient; 

public partial class pages_Del_cthdcd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
        SqlConnection sqlConn = new SqlConnection(ConnectionString);
        try
        {
            sqlConn.Open();
        }
        catch (SqlException ex)
        {
            Response.Write("<script>alert('Kết nối không thành công!')</script>");
        }
        SqlCommand sqlCmd = new SqlCommand();
        sqlCmd.Connection = sqlConn;
        string Id = Request.QueryString["Id"];
        // truy vấn xem dữ liệu
        sqlCmd.CommandText = "Delete from ThiSinh where MaThiSinh = " + Convert.ToInt32(Id);

        try
        {
            sqlCmd.ExecuteNonQuery();
            Response.Redirect("Admin.aspx");
        }
        catch (Exception exn)
        {
            Response.Write("<script>alert('Không xóa được thí sinh này !')</script>");
        }
    }
}