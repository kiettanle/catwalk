﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Home.master" AutoEventWireup="true" CodeFile="the-le-cuoc-thi.aspx.cs" Inherits="pages_the_le_cuoc_thi" EnableEventValidation="false" %>

<%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <img src="../Images/header.jpg" title="Công đoàn trường đại học Sư phạm Kỹ thuật Tp.HCM" alt="#" />
            </div>
            <h2 style="color: darkblue">
                <center>
                    <b>
                     THỂ LỆ THAM GIA CUỘC THI ẢNH ĐẸP CHARMING HCMUTE 2015
                    </b>
                </center>
            </h2>
            <hr />
        </div>
    </div>
    <div class="col-lg-12">
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4 style="font-weight: bold; color: darkblue;">1. Thể lệ cuộc thi:
                        <a href="https://drive.google.com/file/d/0B2i-re2Mmn_YSWw1c3pqRlBqbjQ/view?usp=sharing" target='_blank'
                            title="Thể lệ cuộc thi">Xem thể lệ cuộc thi tại đây</a>
                    </h4>
                    <h4 style="font-weight: bold; color: darkblue;">2. Cách thức tham gia:</h4>
                    <h4 style="font-weight: bold;text-align: justify;">Sau khi đăng ký thành công, bạn sẽ được cấp một mã số. Bạn hãy ghi nhớ mã số này để đặt tên cho file ảnh dự thi. Đối với thí sinh đăng ký theo cặp (đôi) sẽ có thêm mã số nhóm. Thí sinh đăng ký đơn không cần quan tâm mã số này. Nếu quên mã số của mình:
                            <a href="http://charming.hcmute.edu.vn/Result.aspx" target='_blank'
                                title="Thể lệ cuộc thi">Xem lại danh sách đăng ký</a>
                    </h4>
                    <h4 style="color: red; font-weight: bold">Lưu ý: Mỗi thí sinh/cặp (đôi) được phép gửi tối đa 3 ảnh dự thi.</h4>
                    <h4 style="color:darkblue;font-weight: bold">3. Quy định đặt tên file ảnh:</h4>
                    <h4 style="color: red; font-style: italic; font-weight: bold">1. Cá nhân: [MSTS]-[Họ tên thí sinh]-[Tên ảnh]</h4>
                    <h4 style="font-weight: bold">Ví dụ: Tôi tên: Nguyễn Thị Đẹp, Mã số thí sinh là: 1, 3 file ảnh tôi đặt tên là thidep1.jpg, thidep2.jpg, thidep3.jpg.
                        Thì tôi sẽ đặt tên cho 3 ảnh tham gia dự thi của mình như sau:
                    </h4>
                    <h4 style="color: darkblue; font-weight: bold; font-style: italic">1-nguyenthidep-thidep1.jpg ; 1-nguyenthidep-thidep2.jpg ; 1-nguyenthidep-thidep3.jpg</h4>
                    <h4 style="color: red; font-style: italic; font-weight: bold;text-align: justify;">2.  Cặp (Đôi): [MSTS 1]-[MSTS 2]-[Mã nhóm]-[Họ tên thí sinh 1]-[Họ tên thí sinh 2]-[Tên ảnh]</h4>
                    <h4 style="font-weight: bold">Ví dụ: Thí sinh 1: Nguyễn Thị Đẹp, Mã số thí sinh là: 2. Thí sinh 2: Nguyễn Thị Xấu, Mã số thí sinh là: 3. Mã nhóm là 1. 3 file ảnh tôi đặt tên là xaudep1.jpg, xaudep2.jpg, xaudep3.jpg.
                        Thì tôi sẽ đặt tên cho 3 ảnh tham gia dự thi của cặp (đôi) như sau:
                    </h4>
                    <h4 style="color: darkblue; font-weight: bold; font-style: italic">2-3-1-nguyenthidep-nguyenthixau-xaudep1.jpg ; 2-3-1-nguyenthidep-nguyenthixau-xaudep2.jpg ; 2-3-1-nguyenthidep-nguyenthixau-xaudep3.jpg</h4>
                    <h4 style="color: red; font-weight: bold">File ảnh dự thi vui lòng gửi về hộp thư: charming.hcmute.2015@hcmute.edu.vn ,
                    Hạn chót là: 16h00, 14/11/2015</h4>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</asp:Content>


