﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ChangePass.master" AutoEventWireup="true" CodeFile="Change-Password-Admin.aspx.cs" Inherits="pages_Edit_Pass" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong>ĐỔI MẬT KHẨU</strong>
                </div>
                <div class="panel-body">
                    <form role="form" action="#" method="POST">
                        <fieldset>
                            <div class="row">
                                <div class="center-block">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12  col-md-offset-2 ">

                                    <!-------------------->
                                    <div class="form-group">
                                        <div class="input-group">
                                            <p>Họ và tên</p>
                                            <asp:TextBox ID="txt_Name" runat="server" class="form-control"
                                                 type="text" placeholder="Làm sao lấy tên cũ cũ" autofocus Enabled="False"></asp:TextBox>

                                        </div>
                                    </div>
                                    <!------------------>
                                    
                                    <!------------------>
                                    
                                    <!------------------>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <p>Mật khẩu cũ</p>
                                            <asp:TextBox ID="txt_PassCu" runat="server" class="form-control" placeholder="Mật khẩu cũ ..." type="password" autofocus required></asp:TextBox>

                                        </div>
                                    </div>
                                    <!------------------>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <p>Mật Khẩu mới</p>
                                            <asp:TextBox ID="txt_pass_new" runat="server" class="form-control" placeholder="Mật khẩu mới ..." type="password" required></asp:TextBox>

                                        </div>
                                    </div>
                                    <!------------------>

                                    <div class="form-group">
                                        <asp:Button ID="btnSuaPass" runat="server" Text="Đổi mật khẩu"
                                            class="btn btn-primary" OnClick="btnSuaPass_Click" />
                                        <a href="Admin.aspx" class="btn btn-danger">Thoát</a>
                                    </div>
                                    <!-------------------->
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    <div class="notice">
                        <asp:Label ID="lbtNotice" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    </div>
                </div>
                <div class="panel-footer">
                    <asp:Label ID="lbtSaying" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

