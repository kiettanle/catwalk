﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/admin.master" AutoEventWireup="true" CodeFile="Admin.aspx.cs" Inherits="pages_Admin" EnableEventValidation="false" %>
<%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!--Start row-->
     <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <img src="../Images/header.jpg" title="Công đoàn trường đại học Sư phạm Kỹ thuật Tp.HCM" alt="#" />
            </div>
            <h2 style="color: darkblue">
                <center>
                    <b>
                    KẾT QUẢ ĐĂNG ĐĂNG KÝ CUỘC THI ẢNH ĐẸP CHARMING HCMUTE 2015
                    </b>
                </center>
            </h2>
            <hr />
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th class="cus-header" colspan="2">Thông tin đội</th>
                        <th class="cus-header" colspan="9">Thông tin thí sinh</th>
                        <th class="cus-header" colspan="2">Cập nhật</th>

                    </tr>
                    <tr>

                        <th class="cus-header">Đội</th>
                        <th class="cus-header">Tên đội</th>
                        <th class="cus-header">MSTS</th>
                        <th class="cus-header">Họ tên</th>
                        <th class="cus-header">Giới tính</th>
                        <th class="cus-header">Năm sinh</th>
                        <th class="cus-header">Chiều cao</th>
                        <th class="cus-header">Cân nặng</th>
                        <th class="cus-header">Email</th>
                        <th class="cus-header">Điện thoại</th>
                        <th class="cus-header">Thời gian đăng ký</th>
                        <th class="cus-header">Sửa</th>
                        <th class="cus-header">Xoá</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <asp:Repeater ID="Repter_Details" runat="server">
                            <ItemTemplate>
                                <tr>

                                    <td class="text-center"><%# DataBinder.Eval(Container.DataItem, "GroupID")%></td>
                                    <td class="Text"><%# DataBinder.Eval(Container.DataItem, "TenDoi")%></td>
                                    <td class="text-center"><%# DataBinder.Eval(Container.DataItem, "MaThiSinh")%></td>
                                    <td class="text"><%# DataBinder.Eval(Container.DataItem, "HoTen")%></td>
                                    <td class="text-center"><%# DataBinder.Eval(Container.DataItem, "GioiTinh")%></td>
                                    <td class="text-center"><%# DataBinder.Eval(Container.DataItem, "NamSinh")%></td>
                                    <td class="text-center"><%# DataBinder.Eval(Container.DataItem, "ChieuCao")%></td>
                                    <td class="text-center"><%# DataBinder.Eval(Container.DataItem, "CanNang")%></td>
                                    <td class="text"><%# DataBinder.Eval(Container.DataItem, "Email")%></td>
                                    <td class="text-center"><%# DataBinder.Eval(Container.DataItem, "SoDienThoai")%></td>
                                    <td class="text-center"><%# DataBinder.Eval(Container.DataItem, "ThoiGianDangKy")%></td>
                                    <td align="center"><a href="sua-thong-tin-thi-sinh.aspx?id=<%# DataBinder.Eval(Container.DataItem, "MaThiSinh")%>">Sửa</a>
                                    <td class="text-center"><a href="xoa-thi-sinh.aspx?Id=<%# DataBinder.Eval(Container.DataItem,"MaThiSinh") %>" onclick="return confirm('Bạn muốn xóa thí sinh <%# DataBinder.Eval(Container.DataItem,"HoTen") %> ?')"">Xóa</a></td> 
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
        <cc1:CollectionPager ID="clPager" runat="server" BackText="« Trước"
        FirstText="Đầu" LabelText="Trang:" LastText="Cuối" NextText="Sau »"
        ResultsFormat="Hiển thị {0}-{1} (of {2})">
    </cc1:CollectionPager>
</asp:Content>

