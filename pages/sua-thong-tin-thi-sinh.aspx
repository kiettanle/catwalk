﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/admin.master" AutoEventWireup="true" CodeFile="sua-thong-tin-thi-sinh.aspx.cs" Inherits="pages_sua_thong_tin_thi_sinh" EnableEventValidation="false" %>

<%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--row -->
    <div class="row">
        <div class="col-lg-4 col-lg-push-4">
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading text-center" style="color: white; font-weight: bold">SỬA THÔNG TIN THÍ SINH</div>
                    <div class="panel-body">
                        <div class="form">
                            <div>
                                <asp:Label
                                    ID="hoten"
                                    Text="Họ và tên thí sinh"
                                    runat="server"
                                    Font-Bold="true"
                                    ForeColor="darkblue"
                                    Font-Size="Large" />
                                <asp:TextBox ID="txtHoTen" runat="server" placeholder="Họ tên của bạn..." CssClass="form-control" required autofocus></asp:TextBox>
                                <asp:Label
                                    ID="namsinh"
                                    Text="Năm sinh"
                                    runat="server"
                                    Font-Bold="true"
                                    ForeColor="darkblue"
                                    Font-Size="Large" />
                                <asp:TextBox ID="txtNamSinh" runat="server" placeholder="Năm sinh của bạn..." type="number"  CssClass="form-control" required></asp:TextBox>
                                <asp:Label
                                    ID="gioitinh"
                                    Text="Giới tính"
                                    runat="server"
                                    Font-Bold="true"
                                    ForeColor="darkblue"
                                    Font-Size="Large" />
                                <br />
                                <asp:RadioButton
                                    ID="radNam"
                                    runat="server"
                                    Text="Nam"
                                    GroupName="GioiTinh"
                                    Font-Bold="true"
                                    ForeColor="Navy" />
                                &nbsp;&nbsp;&nbsp;
                                <asp:RadioButton
                                    ID="radNu"
                                    runat="server"
                                    Text="Nữ"
                                    GroupName="GioiTinh"
                                    Font-Bold="true"
                                    ForeColor="Navy" />
                                    <br />
                                    <asp:Label
                                    ID="Label1"
                                    Text="Chiều cao"
                                    runat="server"
                                    Font-Bold="true"
                                    ForeColor="darkblue"
                                    Font-Size="Large" />
                                <asp:TextBox ID="txtChieuCao" runat="server" placeholder="Chiều cao..." type="number" Min="100" Max="250" step="0.1" CssClass="form-control" required></asp:TextBox>
                                    <asp:Label
                                    ID="Label2"
                                    Text="Cân nặng"
                                    runat="server"
                                    Font-Bold="true"
                                    ForeColor="darkblue"
                                    Font-Size="Large" />
                                <asp:TextBox ID="txtCanNang" runat="server" placeholder="Cân nặng..." type="number" Min="40" step="0.1" CssClass="form-control" required></asp:TextBox>
                                <asp:Label
                                    ID="Email"
                                    Text="Email"
                                    runat="server"
                                    Font-Bold="true"
                                    ForeColor="darkblue"
                                    Font-Size="Large" />
                                <asp:TextBox ID="txtEmail" type="email" runat="server" placeholder="Nhập email của bạn..." CssClass="form-control" required></asp:TextBox>

                                <asp:Label
                                    ID="sodienthoai"
                                    Text="Số điện thoại"
                                    runat="server"
                                    Font-Bold="true"
                                    ForeColor="darkblue"
                                    Font-Size="Large" />
                                <asp:TextBox ID="txtSoDienThoai" type="text" runat="server" placeholder="Số điện thoại..." CssClass="form-control" required></asp:TextBox>
                                <br />
                                <asp:Button ID="btnSua" runat="server" type="submit" Text="Sửa" OnClick="btnSua_Click" class="btn-success btn-lg col-lg-4" >
                                </asp:Button>
                                 <a href="Admin.aspx" class="btn btn-danger btn-lg col-lg-4 pull-right">Thoát</a> 

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</asp:Content>


