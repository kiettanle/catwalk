﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using EXCEL = Microsoft.Office.Interop.Excel;

public partial class MasterPage_Result : System.Web.UI.MasterPage
{
    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
    SqlConnection sqlConn = new SqlConnection();
    protected void Page_Load(object sender, EventArgs e)
    {
        sqlConn.ConnectionString = ConnectionString;
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            const int NUMBER_ROW = 2;
            EXCEL.Application xl = new EXCEL.Application();
            EXCEL.Workbook wb;
            EXCEL.Worksheet ws = new EXCEL.Worksheet();
            wb = xl.Workbooks.Add(EXCEL.XlWBATemplate.xlWBATWorksheet);
            ws = (EXCEL.Worksheet)wb.Worksheets[1];
            ws.Name = "Test";

            //Content
            ws.Range["B1:D500"].EntireColumn.ColumnWidth = 30;
            ws.Range["A1"].Value = "Title test";
            ws.Range["A1"].EntireRow.Font.Bold = true;
            DataTable dt = GetDatafromDatabase();

            char ch = 'A';
            foreach (DataColumn dtcol in dt.Columns)
            {
                ws.Range[ch.ToString() + 1].Value = dtcol.ColumnName;
                ch++;
            }

            int row = NUMBER_ROW;
            foreach (DataRow dr in dt.Rows)
            {
                ch = 'A';
                for (int col = ch; col < dt.Columns.Count + 65; col++)
                {
                    ws.Range[ch.ToString() + row].Value = dr[col - 65];
                    ch++;
                }
                row++;
            }
            //Borders
            ws.Range["A1" + ":" + ch + row].Borders.LineStyle = EXCEL.XlLineStyle.xlContinuous;

            //Save
            string path = "D:\\" + new Random().Next(99999999) + ".xlsx";
            wb.SaveAs(path);
            xl.Quit();

            System.Threading.Thread.Sleep(1000);

            String FileName = "DanhSachThiSinh.xlsx";
            String FilePath = path;
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "text/plain";
            response.AddHeader("Content-Disposition", "attachment; filename=" + FileName + ";");
            response.TransmitFile(FilePath);
            response.Flush();
            //Xoá file tạm
            File.Delete(path);
            response.End();
        }
        catch(Exception ex)
        {

        }
    }
    protected DataTable GetDatafromDatabase()
    {
        DataTable dt = new DataTable();
        try
        {
            using (sqlConn)
            {
                sqlConn.Open();
                SqlCommand cmd = new SqlCommand("Select MaThiSinh as N'Mã thí sinh',HoTen as N'Họ tên thí sinh',NamSinh as N'Năm sinh',GioiTinh as N'Giới tính',Email as N'Email',SoDienThoai as N'Điện thoại',TenDonVi as N'Đơn vị',ThoiGianDangKy as N'Thời gian đăng ký' from ThiSinh ts inner join DonVi dv on ts.MaDonVi=dv.MaDonVi", sqlConn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                sqlConn.Close();
            }
        }
        catch(Exception ex)
        {

        }
        return dt;
    }
}
