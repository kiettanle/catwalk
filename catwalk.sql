USE [master]
GO
/****** Object:  Database [CatWalk]    Script Date: 11/02/2015 13:01:51 ******/
CREATE DATABASE [CatWalk]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CatWalk', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\CatWalk.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CatWalk_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\CatWalk_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CatWalk] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CatWalk].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CatWalk] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CatWalk] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CatWalk] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CatWalk] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CatWalk] SET ARITHABORT OFF 
GO
ALTER DATABASE [CatWalk] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CatWalk] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CatWalk] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CatWalk] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CatWalk] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CatWalk] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CatWalk] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CatWalk] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CatWalk] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CatWalk] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CatWalk] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CatWalk] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CatWalk] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CatWalk] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CatWalk] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CatWalk] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CatWalk] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CatWalk] SET RECOVERY FULL 
GO
ALTER DATABASE [CatWalk] SET  MULTI_USER 
GO
ALTER DATABASE [CatWalk] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CatWalk] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CatWalk] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CatWalk] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [CatWalk] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CatWalk', N'ON'
GO
USE [CatWalk]
GO
/****** Object:  Table [dbo].[DonVi]    Script Date: 11/02/2015 13:01:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DonVi](
	[MaDonVi] [int] NOT NULL,
	[TenDonVi] [nvarchar](100) NULL,
 CONSTRAINT [PK_DonVi] PRIMARY KEY CLUSTERED 
(
	[MaDonVi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ThiSinh]    Script Date: 11/02/2015 13:01:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThiSinh](
	[MaThiSinh] [int] IDENTITY(1,1) NOT NULL,
	[HoTen] [nvarchar](50) NULL,
	[GioiTinh] [nvarchar](3) NULL,
	[NamSinh] [nvarchar](50) NULL,
	[ChieuCao] [nvarchar](50) NULL,
	[CanNang] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[SoDienThoai] [nvarchar](50) NULL,
	[ThoiGianDangKy] [datetime] NULL,
	[GroupID] [int] NULL,
	[TenDoi] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 11/02/2015 13:01:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [int] NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (1, N'Ban Quản Lý Ký Túc Xá')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (2, N'Khoa Cơ Khí Động Lực ')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (3, N'Khoa Cơ Khí Máy')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (4, N'Khoa Công Nghệ Hoá Học & Thực Phẩm')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (5, N'Khoa Công Nghệ May & Thời Trang')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (6, N'Khoa Công Nghệ Thông Tin')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (7, N'Khoa Đào Tạo Chất Lượng Cao')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (8, N'Khoa Điện - Điện Tử')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (9, N'Khoa In & Truyền Thông')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (10, N'Khoa Khoa Học Cơ Bản')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (11, N'Khoa Kinh Tế')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (12, N'Khoa Lý Luận Chính Trị')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (13, N'Khoa Ngoại Ngữ')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (14, N'Khoa Xây Dựng')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (15, N'Phòng Công Tác HSSV & QHCC')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (16, N'Phòng Đào Tạo')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (17, N'Phòng Đào Tạo Không Chính Quy')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (18, N'Phòng Hành Chính Tổng Hợp & Trung Tâm DVSV')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (19, N'Phòng Kế Hoạch Tài Chính')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (20, N'Phòng Quản Lý Khoa Học & Quan Hệ Quốc Tế')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (21, N'Phòng QTCL & TTTTMT')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (22, N'Phòng Quản Trị Cơ Sở Vật Chất')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (23, N'Phòng Thanh Tra Giáo Dục & Đảm Bảo Chất Lượng')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (24, N'Phòng Thiết Bị Vật Tư')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (25, N'Phòng Tổ Chức Cán Bộ & Trung Tâm Dạy Học Số')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (26, N'Thư Viện - Trạm Y Tế')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (27, N'Trung Tâm Hợp Tác Đào Tạo Quốc Tế')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (28, N'Trung Tâm Kỹ Thuật Tổng Hợp')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (29, N'Trung Tâm Việt Đức')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (30, N'Trường THKTTH')
INSERT [dbo].[DonVi] ([MaDonVi], [TenDonVi]) VALUES (31, N'Viện Sư Phạm Kỹ Thuật')
SET IDENTITY_INSERT [dbo].[ThiSinh] ON 

INSERT [dbo].[ThiSinh] ([MaThiSinh], [HoTen], [GioiTinh], [NamSinh], [ChieuCao], [CanNang], [Email], [SoDienThoai], [ThoiGianDangKy], [GroupID], [TenDoi]) VALUES (1, N'0', NULL, N'0', N'0', N'0', N'0', N'0', CAST(N'2015-11-02 12:51:05.780' AS DateTime), 0, N'0')
INSERT [dbo].[ThiSinh] ([MaThiSinh], [HoTen], [GioiTinh], [NamSinh], [ChieuCao], [CanNang], [Email], [SoDienThoai], [ThoiGianDangKy], [GroupID], [TenDoi]) VALUES (2, N'Kiệt', N'Nữ', N'1950', N'101', N'40.2', N'kiet1@hcmute.edu.vn', N'0987654321', CAST(N'2015-11-02 12:51:12.997' AS DateTime), 1, N'Những Bông Hoa Rực Lửa')
INSERT [dbo].[ThiSinh] ([MaThiSinh], [HoTen], [GioiTinh], [NamSinh], [ChieuCao], [CanNang], [Email], [SoDienThoai], [ThoiGianDangKy], [GroupID], [TenDoi]) VALUES (3, N'Kiệt', N'Nữ', N'1950', N'103', N'40.3', N'kiet2@hcmute.edu.vn', N'0987654321', CAST(N'2015-11-02 12:51:13.000' AS DateTime), 1, N'Những Bông Hoa Rực Lửa')
SET IDENTITY_INSERT [dbo].[ThiSinh] OFF
INSERT [dbo].[Users] ([UserID], [UserName], [Email], [Password]) VALUES (1, N'Nguyễn Nam Thắng', N'thangspkt@gmail.com', N'thang@123')
USE [master]
GO
ALTER DATABASE [CatWalk] SET  READ_WRITE 
GO
